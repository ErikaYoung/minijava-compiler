/**
 * @program: minijava-compiler
 * @description:
 * @author: Erika
 * @create: 2018-12-23 00:31
 **/


import gen.MiniJavaGrammarLexer;
import gen.MiniJavaGrammarParser;
import org.antlr.v4.gui.TreeViewer;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.util.Arrays;

public class CompilerGUI {
    public static void main(String[] args) throws IOException {
        CharStream stream = new ANTLRInputStream(new FileReader(System.getProperty("user.dir")+"/src/test/testwrong.txt"));
        MiniJavaGrammarLexer lexer = new MiniJavaGrammarLexer(stream);
        TokenStream tokenStream = new CommonTokenStream(lexer);
        MiniJavaGrammarParser parser = new MiniJavaGrammarParser(tokenStream);
        ParseTree tree = parser.goal();

        //show AST in console
        System.out.println(tree.toStringTree(parser));

        TreeViewer viewr = new TreeViewer(Arrays.asList(parser.getRuleNames()), tree);
        viewr.setScale(1);//scale a little
        viewr.open();

    }
}
