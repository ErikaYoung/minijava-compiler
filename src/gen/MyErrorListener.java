package gen;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.RuleContext;
import org.antlr.v4.runtime.Token;
import gen.MiniJavaGrammarParser.*;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNode;
import sun.applet.Main;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: minijava-compiler
 * @description:
 * @author: Erika
 * @create: 2018-12-23 15:19
 **/
public class MyErrorListener  {
    public static void myPrintln(String errorMessage,Token token){
        if(token!=null)
            System.out.println("Error:("+token.getLine()+", "+token.getCharPositionInLine()+") MiniJava:"+errorMessage);
    }

    public static void isInteger(String type,Token token){
        if(type == null || !type.equals("int"))
            myPrintln("Index is not an integer",token);
    }

    public static void validIntegerOperation(String a,String b,Token token){
        if (a == null || b == null || !a.equals("int") || !b.equals("int"))
            myPrintln("Illegal input for integer operation:" + token.getText(), token);
    }

    public static void validBooleanOperation(String a,String b,Token token){
        if (a == null || b == null || !a.equals("boolean") || !b.equals("boolean"))
            myPrintln("Illegal input for boolean operation:" + token.getText(), token);
    }

    public static void validLengthOperation(String a,Token token){
        if(a==null || !a.equals("int[]"))
            myPrintln("Illegal input for '.length'" , token);
    }

    public static void validArrayVariable(String a,Token token){
            if(a==null || !a.equals("int[]"))
                myPrintln("Unsupported array type: "+a, token);
    }

    public static ParameterDeclarationContext getVariableDeclarationFromClass(ClassDeclarationContext className, String varName){
        ParameterDeclarationContext v =null;
        List<VarDeclarationStatementContext> var_list = className.varDeclarationStatement();
        for(VarDeclarationStatementContext e:var_list)	{
            if (e.parameterDeclaration().varName.equals(varName)){
                v = e.parameterDeclaration();
                break;
            }
        }
        return v;
    }

    public static ParameterDeclarationContext getVariableDeclarationFromMethod(MethodDeclarationContext method,String varName){
        ParameterDeclarationContext v =null;
        List<ParameterDeclarationContext> param_list = method.parameterDeclaration();
        List<VarDeclarationStatementContext> var_list = method.varDeclarationStatement();
        for(ParameterDeclarationContext e:param_list)	{
            if (e.varName.equals(varName)){
                v = e;
                return v;
            }
        }
        for(VarDeclarationStatementContext e:var_list){
            if (e.parameterDeclaration().varName.equals(varName)){
                v = e.parameterDeclaration();
                return v;
            }
        }
        return v;
    }

    public static void checkVariableDefined(ParserRuleContext variable,Token token){
        ParserRuleContext savedVariable = variable;
        if(variable instanceof ParameterDeclarationContext){
            String name = ((ParameterDeclarationContext)variable).varName;
            ParameterDeclarationContext parameterDeclarationContext = null;
            if(variable.getParent() instanceof VarDeclarationStatementContext)
                variable = variable.getParent();
            if(variable.getParent() instanceof MethodDeclarationContext){
                MethodDeclarationContext method = (MethodDeclarationContext) variable.getParent();
                parameterDeclarationContext = getVariableDeclarationFromMethod(method,name);
                variable = variable.getParent();
            }
            if((parameterDeclarationContext ==null || parameterDeclarationContext == savedVariable) && variable.getParent() instanceof ClassDeclarationContext){
                ClassDeclarationContext classDeclarationContext = (ClassDeclarationContext) variable.getParent();
                parameterDeclarationContext = getVariableDeclarationFromClass(classDeclarationContext,name);
            }
            if(parameterDeclarationContext!=null && parameterDeclarationContext!=savedVariable)
                myPrintln("Variable "+name+" has been defined on line "+parameterDeclarationContext.IDENTIFIER.getLine(),token);
        }
    }

    public static ParameterDeclarationContext getVariableDeclarationFromExpression(ParserRuleContext expression,Token token){
        ParameterDeclarationContext result = null;
        ParserRuleContext parent = expression.getParent();
        while(parent instanceof StatementContext || parent instanceof ExpressionContext || parent instanceof ArrayIndexContext){
            parent = parent.getParent();
        }
        while(result == null && parent instanceof MethodDeclarationContext){
            result = getVariableDeclarationFromMethod((MethodDeclarationContext)parent,token.getText());
            parent = parent.getParent();
        }
        while(result == null && parent instanceof ClassDeclarationContext){
            result = getVariableDeclarationFromClass((ClassDeclarationContext)parent,token.getText());
            parent = parent.getParent();
        }

        return result;
    }


    public static String checkDefined(ParameterDeclarationContext context,Token token){
        if(context == null) {
            myPrintln("Undefined variable: " + token.getText(), token);
            return null;
        }
        else {
            return context.varType;
        }
    }

    public static void assignStatementCheck(String variableType,String b, Token token){
        if(variableType==null);
        else{
            if(!variableType.equals(b)){
                myPrintln("Assigning "+b+" to "+variableType+" varaible ",token);
            }
        }
    }

    public static String getCurrentClass(ParserRuleContext expression){
        while(expression!=null && !(expression instanceof ClassDeclarationContext)){
            expression = expression.getParent();
        }
        if(expression instanceof ClassDeclarationContext)
            return ((ClassDeclarationContext)expression).name;
        else
            return null;
    }

    public static ClassDeclarationContext getClassDeclaration(ParserRuleContext context,String className,Token token){
        while(context!=null && !(context instanceof GoalContext)){
            context = context.getParent();
        }
        if(context!=null){
            List<ClassDeclarationContext> class_list = ((GoalContext)context).classDeclaration();
            MainClassContext mainClassContext = ((GoalContext)context).mainClass();
            if(!mainClassContext.className.getText().equals(className)) {
                for (ClassDeclarationContext c : class_list) {
                    if (c.name.equals(className))
                        return c;
                }
                myPrintln("Undefined class: " + className, token);
            }
        }
        else
            myPrintln("Undefined class: "+className,token);
        return null;
    }


    public static String checkDefinedMethod(ParserRuleContext expression,Token token){
        if(expression instanceof ExpressionContext){
            String className = ((ExpressionContext) expression).var.expressionType;
            List<ExpressionContext> param_list = ((ExpressionContext) expression).expression();
            for(int i=0;i<param_list.size();i++);
                //System.out.println(param_list.get(i).expressionType+" "+param_list.get(i).IDENTIFIER);
            String methodName = ((ExpressionContext) expression).IDENTIFIER.getText();
            // get class
            ClassDeclarationContext classContext = getClassDeclaration(expression,className,token);
            if(classContext!=null) {
                List<MethodDeclarationContext> method_list = classContext.methodDeclaration();
                //traverse all the method of the class
                nextOne:for (MethodDeclarationContext m : method_list) {
                    if(m.method.varName.equals(methodName)){
                        List<ParameterDeclarationContext> param_method_list = m.parameterDeclaration();
                        // check if the param list is the same
                        if(param_list.size()==param_method_list.size()){
                            for(int i=0;i<param_list.size()-1;i++){
                                //System.out.println(param_list.get(i).expressionType);
                                if(!param_list.get(i+1).expressionType.equals(param_method_list.get(i).varType))
                                    continue nextOne;
                            }

                            return m.method.varType;
                        }
                    }
                }
                myPrintln("Undefined method: "+methodName,token);
            }
        }
        return null;
    }



    public static void returnTypeCheck(String a,String b,Token c){
        if(!a.equals(b)){
            myPrintln("Wrong return type: "+b+" Should be type: "+a,c);
        }
    }

    public static void getAllExpression(ParserRuleContext node){
        if(node == null)
            return;
        if(node instanceof ExpressionContext){
            if(((ExpressionContext) node).var!=null) {
                ((ExpressionContext) node).expressionType = checkDefinedMethod(node, ((ExpressionContext) node).IDENTIFIER);
            }
            return;
        }
        for(ParseTree p:node.children){
            if(p instanceof ParserRuleContext)
                getAllExpression((ParserRuleContext) p );
        }
    }

    public static void goThroughExpression(ParserRuleContext node){
        if(node == null)
            return;
        if(node instanceof ExpressionContext){
            return;
        }
        for(ParseTree p:node.children){
            if(p instanceof ParserRuleContext)
                getAllExpression((ParserRuleContext) p );
        }
    }

    public static void checkClassName(ClassDeclarationContext context,Token token){
        if(context.getParent() instanceof GoalContext){
            GoalContext goal = (GoalContext) context.getParent();
            if(goal.mainClass().className.getText().equals(context.name)){
                myPrintln("Class name "+context.name +" has been defined in main class",token);
            }
            for(ClassDeclarationContext c:goal.classDeclaration()){
                if(c.name.equals(context.name) && c!=context)
                    myPrintln("Class name "+context.name+" has been defined on line "+c.IDENTIFIER.getLine(),token);
            }
        }
    }

    public static void methodOverrideCheck(ParserRuleContext context,Token token){
        MethodDeclarationContext methodDeclarationContext = (MethodDeclarationContext) context;
        List<ParameterDeclarationContext> p = methodDeclarationContext.parameterDeclaration();
        ClassDeclarationContext classDeclarationContext = (ClassDeclarationContext) context.getParent();
        List<MethodDeclarationContext> methods = classDeclarationContext.methodDeclaration();
        m:for(MethodDeclarationContext method:methods){
            if(methodDeclarationContext.method.varName.equals(method.method.varName)&& method.c.getLine()!=methodDeclarationContext.c.getLine() ){
                List<ParameterDeclarationContext> p1 = method.parameterDeclaration();
                if(p1.size() == p.size() ){
                    for(int i=1;i<p.size();i++){
                        if(!p1.get(i).varType.equals(p.get(i).varType))
                            continue m;
                    }
                    myPrintln("Method "+method.method.varName+" has been defined on line "+method.c.getLine(),token);
                }
            }
        }
    }
}

