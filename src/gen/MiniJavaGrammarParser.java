// Generated from C:/Users/erika/Videos/CS/����/minijava-compiler/src\MiniJavaGrammar.g4 by ANTLR 4.7
package gen;

	import gen.MyErrorListener;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class MiniJavaGrammarParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, T__20=21, T__21=22, T__22=23, T__23=24, 
		T__24=25, T__25=26, T__26=27, T__27=28, T__28=29, T__29=30, T__30=31, 
		SPACE=32, ONELINE_COMMENT=33, MULTIPLELINE_COMMENT=34, INTEGER_OPERATOR=35, 
		BOOLEAN_OPERATOR=36, IDENTIFIER=37, INTEGER=38;
	public static final int
		RULE_goal = 0, RULE_packageDeclaration = 1, RULE_mainClass = 2, RULE_classDeclaration = 3, 
		RULE_parameterDeclaration = 4, RULE_varDeclarationStatement = 5, RULE_methodDeclaration = 6, 
		RULE_expression = 7, RULE_type = 8, RULE_statement = 9, RULE_arrayIndex = 10;
	public static final String[] ruleNames = {
		"goal", "packageDeclaration", "mainClass", "classDeclaration", "parameterDeclaration", 
		"varDeclarationStatement", "methodDeclaration", "expression", "type", 
		"statement", "arrayIndex"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'package'", "','", "';'", "'class'", "'{'", "'public'", "'static'", 
		"'void'", "'main'", "'('", "'String'", "'['", "']'", "')'", "'}'", "'extends'", 
		"'return'", "'.'", "'length'", "'true'", "'false'", "'this'", "'new'", 
		"'int'", "'!'", "'boolean'", "'if'", "'else'", "'while'", "'System.out.println'", 
		"'='", null, null, null, null, "'&&'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, "SPACE", "ONELINE_COMMENT", 
		"MULTIPLELINE_COMMENT", "INTEGER_OPERATOR", "BOOLEAN_OPERATOR", "IDENTIFIER", 
		"INTEGER"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "MiniJavaGrammar.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public MiniJavaGrammarParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class GoalContext extends ParserRuleContext {
		public MainClassContext mainClass() {
			return getRuleContext(MainClassContext.class,0);
		}
		public TerminalNode EOF() { return getToken(MiniJavaGrammarParser.EOF, 0); }
		public PackageDeclarationContext packageDeclaration() {
			return getRuleContext(PackageDeclarationContext.class,0);
		}
		public List<ClassDeclarationContext> classDeclaration() {
			return getRuleContexts(ClassDeclarationContext.class);
		}
		public ClassDeclarationContext classDeclaration(int i) {
			return getRuleContext(ClassDeclarationContext.class,i);
		}
		public GoalContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_goal; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MiniJavaGrammarListener ) ((MiniJavaGrammarListener)listener).enterGoal(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MiniJavaGrammarListener ) ((MiniJavaGrammarListener)listener).exitGoal(this);
		}
	}

	public final GoalContext goal() throws RecognitionException {
		GoalContext _localctx = new GoalContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_goal);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(23);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__0) {
				{
				setState(22);
				packageDeclaration();
				}
			}

			setState(25);
			mainClass();
			setState(29);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__3) {
				{
				{
				setState(26);
				classDeclaration();
				}
				}
				setState(31);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(32);
			match(EOF);

			        MyErrorListener.getAllExpression(_localctx);
			      
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PackageDeclarationContext extends ParserRuleContext {
		public List<TerminalNode> IDENTIFIER() { return getTokens(MiniJavaGrammarParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(MiniJavaGrammarParser.IDENTIFIER, i);
		}
		public PackageDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_packageDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MiniJavaGrammarListener ) ((MiniJavaGrammarListener)listener).enterPackageDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MiniJavaGrammarListener ) ((MiniJavaGrammarListener)listener).exitPackageDeclaration(this);
		}
	}

	public final PackageDeclarationContext packageDeclaration() throws RecognitionException {
		PackageDeclarationContext _localctx = new PackageDeclarationContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_packageDeclaration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(35);
			match(T__0);
			setState(36);
			match(IDENTIFIER);
			setState(41);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__1) {
				{
				{
				setState(37);
				match(T__1);
				setState(38);
				match(IDENTIFIER);
				}
				}
				setState(43);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(44);
			match(T__2);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MainClassContext extends ParserRuleContext {
		public Token className;
		public List<TerminalNode> IDENTIFIER() { return getTokens(MiniJavaGrammarParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(MiniJavaGrammarParser.IDENTIFIER, i);
		}
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public MainClassContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_mainClass; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MiniJavaGrammarListener ) ((MiniJavaGrammarListener)listener).enterMainClass(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MiniJavaGrammarListener ) ((MiniJavaGrammarListener)listener).exitMainClass(this);
		}
	}

	public final MainClassContext mainClass() throws RecognitionException {
		MainClassContext _localctx = new MainClassContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_mainClass);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(46);
			match(T__3);
			setState(47);
			((MainClassContext)_localctx).className = match(IDENTIFIER);
			setState(48);
			match(T__4);
			setState(49);
			match(T__5);
			setState(50);
			match(T__6);
			setState(51);
			match(T__7);
			setState(52);
			match(T__8);
			setState(53);
			match(T__9);
			setState(54);
			match(T__10);
			setState(55);
			match(T__11);
			setState(56);
			match(T__12);
			setState(57);
			match(IDENTIFIER);
			setState(58);
			match(T__13);
			setState(59);
			match(T__4);
			setState(63);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__4) | (1L << T__26) | (1L << T__28) | (1L << T__29) | (1L << IDENTIFIER))) != 0)) {
				{
				{
				setState(60);
				statement();
				}
				}
				setState(65);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(66);
			match(T__14);
			setState(67);
			match(T__14);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ClassDeclarationContext extends ParserRuleContext {
		public String name;
		public Token IDENTIFIER;
		public Token extendClass;
		public List<TerminalNode> IDENTIFIER() { return getTokens(MiniJavaGrammarParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(MiniJavaGrammarParser.IDENTIFIER, i);
		}
		public List<VarDeclarationStatementContext> varDeclarationStatement() {
			return getRuleContexts(VarDeclarationStatementContext.class);
		}
		public VarDeclarationStatementContext varDeclarationStatement(int i) {
			return getRuleContext(VarDeclarationStatementContext.class,i);
		}
		public List<MethodDeclarationContext> methodDeclaration() {
			return getRuleContexts(MethodDeclarationContext.class);
		}
		public MethodDeclarationContext methodDeclaration(int i) {
			return getRuleContext(MethodDeclarationContext.class,i);
		}
		public ClassDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_classDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MiniJavaGrammarListener ) ((MiniJavaGrammarListener)listener).enterClassDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MiniJavaGrammarListener ) ((MiniJavaGrammarListener)listener).exitClassDeclaration(this);
		}
	}

	public final ClassDeclarationContext classDeclaration() throws RecognitionException {
		ClassDeclarationContext _localctx = new ClassDeclarationContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_classDeclaration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(69);
			match(T__3);
			setState(70);
			((ClassDeclarationContext)_localctx).IDENTIFIER = match(IDENTIFIER);

			                        ((ClassDeclarationContext)_localctx).name =  (((ClassDeclarationContext)_localctx).IDENTIFIER!=null?((ClassDeclarationContext)_localctx).IDENTIFIER.getText():null);
			                        
			setState(74);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__15) {
				{
				setState(72);
				match(T__15);
				setState(73);
				((ClassDeclarationContext)_localctx).extendClass = match(IDENTIFIER);
				}
			}

			setState(76);
			match(T__4);
			setState(80);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__23) | (1L << T__25) | (1L << IDENTIFIER))) != 0)) {
				{
				{
				setState(77);
				varDeclarationStatement();
				}
				}
				setState(82);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(86);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__5) {
				{
				{
				setState(83);
				methodDeclaration();
				}
				}
				setState(88);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(89);
			match(T__14);

			                            MyErrorListener.checkClassName(_localctx,((ClassDeclarationContext)_localctx).IDENTIFIER);
			                            if(((ClassDeclarationContext)_localctx).extendClass!=null)

			                                MyErrorListener.getClassDeclaration(_localctx,((ClassDeclarationContext)_localctx).extendClass.getText(),((ClassDeclarationContext)_localctx).IDENTIFIER);

			                       
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParameterDeclarationContext extends ParserRuleContext {
		public String varType;
		public String varName;
		public TypeContext t;
		public Token IDENTIFIER;
		public TerminalNode IDENTIFIER() { return getToken(MiniJavaGrammarParser.IDENTIFIER, 0); }
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public ParameterDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parameterDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MiniJavaGrammarListener ) ((MiniJavaGrammarListener)listener).enterParameterDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MiniJavaGrammarListener ) ((MiniJavaGrammarListener)listener).exitParameterDeclaration(this);
		}
	}

	public final ParameterDeclarationContext parameterDeclaration() throws RecognitionException {
		ParameterDeclarationContext _localctx = new ParameterDeclarationContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_parameterDeclaration);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(92);
			((ParameterDeclarationContext)_localctx).t = type();
			setState(93);
			((ParameterDeclarationContext)_localctx).IDENTIFIER = match(IDENTIFIER);

			    ((ParameterDeclarationContext)_localctx).varType =  (((ParameterDeclarationContext)_localctx).t!=null?_input.getText(((ParameterDeclarationContext)_localctx).t.start,((ParameterDeclarationContext)_localctx).t.stop):null);
			    _localctx.varName = (((ParameterDeclarationContext)_localctx).IDENTIFIER!=null?((ParameterDeclarationContext)_localctx).IDENTIFIER.getText():null);
			    MyErrorListener.checkVariableDefined(_localctx,((ParameterDeclarationContext)_localctx).IDENTIFIER);

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VarDeclarationStatementContext extends ParserRuleContext {
		public Token a;
		public ParameterDeclarationContext parameterDeclaration() {
			return getRuleContext(ParameterDeclarationContext.class,0);
		}
		public VarDeclarationStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_varDeclarationStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MiniJavaGrammarListener ) ((MiniJavaGrammarListener)listener).enterVarDeclarationStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MiniJavaGrammarListener ) ((MiniJavaGrammarListener)listener).exitVarDeclarationStatement(this);
		}
	}

	public final VarDeclarationStatementContext varDeclarationStatement() throws RecognitionException {
		VarDeclarationStatementContext _localctx = new VarDeclarationStatementContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_varDeclarationStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(96);
			parameterDeclaration();
			setState(97);
			((VarDeclarationStatementContext)_localctx).a = match(T__2);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MethodDeclarationContext extends ParserRuleContext {
		public String returnType;
		public ParameterDeclarationContext method;
		public Token c;
		public ExpressionContext expression;
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public List<ParameterDeclarationContext> parameterDeclaration() {
			return getRuleContexts(ParameterDeclarationContext.class);
		}
		public ParameterDeclarationContext parameterDeclaration(int i) {
			return getRuleContext(ParameterDeclarationContext.class,i);
		}
		public List<VarDeclarationStatementContext> varDeclarationStatement() {
			return getRuleContexts(VarDeclarationStatementContext.class);
		}
		public VarDeclarationStatementContext varDeclarationStatement(int i) {
			return getRuleContext(VarDeclarationStatementContext.class,i);
		}
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public MethodDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_methodDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MiniJavaGrammarListener ) ((MiniJavaGrammarListener)listener).enterMethodDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MiniJavaGrammarListener ) ((MiniJavaGrammarListener)listener).exitMethodDeclaration(this);
		}
	}

	public final MethodDeclarationContext methodDeclaration() throws RecognitionException {
		MethodDeclarationContext _localctx = new MethodDeclarationContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_methodDeclaration);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(99);
			match(T__5);
			setState(100);
			((MethodDeclarationContext)_localctx).method = parameterDeclaration();

			                            ((MethodDeclarationContext)_localctx).returnType =  ((MethodDeclarationContext)_localctx).method.varType;
			                     
			setState(102);
			((MethodDeclarationContext)_localctx).c = match(T__9);
			setState(111);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__23) | (1L << T__25) | (1L << IDENTIFIER))) != 0)) {
				{
				setState(103);
				parameterDeclaration();
				setState(108);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__1) {
					{
					{
					setState(104);
					match(T__1);
					setState(105);
					parameterDeclaration();
					}
					}
					setState(110);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(113);
			match(T__13);

			                            MyErrorListener.methodOverrideCheck(_localctx,((MethodDeclarationContext)_localctx).c);
			                     
			setState(115);
			match(T__4);
			setState(119);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,9,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(116);
					varDeclarationStatement();
					}
					} 
				}
				setState(121);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,9,_ctx);
			}
			setState(125);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__4) | (1L << T__26) | (1L << T__28) | (1L << T__29) | (1L << IDENTIFIER))) != 0)) {
				{
				{
				setState(122);
				statement();
				}
				}
				setState(127);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(128);
			match(T__16);
			setState(129);
			((MethodDeclarationContext)_localctx).expression = expression(0);
			setState(130);
			match(T__2);
			setState(131);
			match(T__14);

			                          MyErrorListener.returnTypeCheck(_localctx.returnType,((MethodDeclarationContext)_localctx).expression.expressionType,((MethodDeclarationContext)_localctx).c);

			                    
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public String expressionType;
		public ExpressionContext exp1;
		public ExpressionContext exp;
		public ExpressionContext var;
		public Token IDENTIFIER;
		public Token token;
		public Token op;
		public ExpressionContext exp2;
		public ArrayIndexContext arr;
		public TerminalNode IDENTIFIER() { return getToken(MiniJavaGrammarParser.IDENTIFIER, 0); }
		public TerminalNode INTEGER() { return getToken(MiniJavaGrammarParser.INTEGER, 0); }
		public ArrayIndexContext arrayIndex() {
			return getRuleContext(ArrayIndexContext.class,0);
		}
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode INTEGER_OPERATOR() { return getToken(MiniJavaGrammarParser.INTEGER_OPERATOR, 0); }
		public TerminalNode BOOLEAN_OPERATOR() { return getToken(MiniJavaGrammarParser.BOOLEAN_OPERATOR, 0); }
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MiniJavaGrammarListener ) ((MiniJavaGrammarListener)listener).enterExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MiniJavaGrammarListener ) ((MiniJavaGrammarListener)listener).exitExpression(this);
		}
	}

	public final ExpressionContext expression() throws RecognitionException {
		return expression(0);
	}

	private ExpressionContext expression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExpressionContext _localctx = new ExpressionContext(_ctx, _parentState);
		ExpressionContext _prevctx = _localctx;
		int _startState = 14;
		enterRecursionRule(_localctx, 14, RULE_expression, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(164);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,11,_ctx) ) {
			case 1:
				{
				setState(135);
				((ExpressionContext)_localctx).IDENTIFIER = match(IDENTIFIER);

				                ParameterDeclarationContext varContext = MyErrorListener.getVariableDeclarationFromExpression(_localctx,((ExpressionContext)_localctx).IDENTIFIER);
				                ((ExpressionContext)_localctx).expressionType =  MyErrorListener.checkDefined(varContext,((ExpressionContext)_localctx).IDENTIFIER);
				            
				}
				break;
			case 2:
				{
				setState(137);
				match(INTEGER);

				                ((ExpressionContext)_localctx).expressionType =  "int";
				            
				}
				break;
			case 3:
				{
				setState(139);
				match(T__19);

				                ((ExpressionContext)_localctx).expressionType =  "boolean";
				            
				}
				break;
			case 4:
				{
				setState(141);
				match(T__20);

				                ((ExpressionContext)_localctx).expressionType =  "boolean";
				            
				}
				break;
			case 5:
				{
				setState(143);
				match(T__21);

				                ((ExpressionContext)_localctx).expressionType =  MyErrorListener.getCurrentClass(_localctx);
				            
				}
				break;
			case 6:
				{
				setState(145);
				match(T__22);
				setState(146);
				match(T__23);
				setState(147);
				arrayIndex();

				                ((ExpressionContext)_localctx).expressionType =  "int[]";
				            
				}
				break;
			case 7:
				{
				setState(150);
				match(T__22);
				setState(151);
				((ExpressionContext)_localctx).token = match(IDENTIFIER);
				setState(152);
				match(T__9);
				setState(153);
				match(T__13);

				                ((ExpressionContext)_localctx).expressionType =  ((ExpressionContext)_localctx).token.getText();
				            
				}
				break;
			case 8:
				{
				setState(155);
				match(T__24);
				setState(156);
				((ExpressionContext)_localctx).exp = expression(2);

				                ((ExpressionContext)_localctx).expressionType =  ((ExpressionContext)_localctx).exp.expressionType;
				            
				}
				break;
			case 9:
				{
				setState(159);
				match(T__9);
				setState(160);
				((ExpressionContext)_localctx).exp = expression(0);
				setState(161);
				match(T__13);

				                ((ExpressionContext)_localctx).expressionType =  ((ExpressionContext)_localctx).exp.expressionType;
				            
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(201);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,15,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(199);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,14,_ctx) ) {
					case 1:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState);
						_localctx.exp1 = _prevctx;
						_localctx.exp1 = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(166);
						if (!(precpred(_ctx, 13))) throw new FailedPredicateException(this, "precpred(_ctx, 13)");
						setState(167);
						((ExpressionContext)_localctx).op = match(INTEGER_OPERATOR);
						setState(168);
						((ExpressionContext)_localctx).exp2 = expression(14);

						                          MyErrorListener.validIntegerOperation(((ExpressionContext)_localctx).exp1.expressionType,((ExpressionContext)_localctx).exp2.expressionType, ((ExpressionContext)_localctx).op);
						                          ((ExpressionContext)_localctx).expressionType =  "int";
						                      
						}
						break;
					case 2:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState);
						_localctx.exp1 = _prevctx;
						_localctx.exp1 = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(171);
						if (!(precpred(_ctx, 12))) throw new FailedPredicateException(this, "precpred(_ctx, 12)");
						setState(172);
						((ExpressionContext)_localctx).op = match(BOOLEAN_OPERATOR);
						setState(173);
						((ExpressionContext)_localctx).exp2 = expression(13);

						                          MyErrorListener.validBooleanOperation(((ExpressionContext)_localctx).exp1.expressionType,((ExpressionContext)_localctx).exp2.expressionType, ((ExpressionContext)_localctx).op);
						                          ((ExpressionContext)_localctx).expressionType =  "boolean";
						                      
						}
						break;
					case 3:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState);
						_localctx.exp = _prevctx;
						_localctx.exp = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(176);
						if (!(precpred(_ctx, 11))) throw new FailedPredicateException(this, "precpred(_ctx, 11)");
						setState(177);
						((ExpressionContext)_localctx).arr = arrayIndex();

						                          MyErrorListener.validArrayVariable(((ExpressionContext)_localctx).exp.expressionType,((ExpressionContext)_localctx).arr.t);
						                          ((ExpressionContext)_localctx).expressionType = "int";
						                      
						}
						break;
					case 4:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState);
						_localctx.exp1 = _prevctx;
						_localctx.exp1 = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(180);
						if (!(precpred(_ctx, 10))) throw new FailedPredicateException(this, "precpred(_ctx, 10)");
						setState(181);
						match(T__17);
						setState(182);
						((ExpressionContext)_localctx).op = match(T__18);

						                          MyErrorListener.validLengthOperation(((ExpressionContext)_localctx).exp1.expressionType, ((ExpressionContext)_localctx).op);
						                          ((ExpressionContext)_localctx).expressionType =  "int";
						                      
						}
						break;
					case 5:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState);
						_localctx.var = _prevctx;
						_localctx.var = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(184);
						if (!(precpred(_ctx, 9))) throw new FailedPredicateException(this, "precpred(_ctx, 9)");
						setState(185);
						match(T__17);
						setState(186);
						((ExpressionContext)_localctx).IDENTIFIER = match(IDENTIFIER);
						setState(187);
						match(T__9);
						setState(196);
						_errHandler.sync(this);
						_la = _input.LA(1);
						if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__9) | (1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << T__22) | (1L << T__24) | (1L << IDENTIFIER) | (1L << INTEGER))) != 0)) {
							{
							setState(188);
							expression(0);
							setState(193);
							_errHandler.sync(this);
							_la = _input.LA(1);
							while (_la==T__1) {
								{
								{
								setState(189);
								match(T__1);
								setState(190);
								expression(0);
								}
								}
								setState(195);
								_errHandler.sync(this);
								_la = _input.LA(1);
							}
							}
						}

						setState(198);
						match(T__13);
						}
						break;
					}
					} 
				}
				setState(203);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,15,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class TypeContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(MiniJavaGrammarParser.IDENTIFIER, 0); }
		public TypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MiniJavaGrammarListener ) ((MiniJavaGrammarListener)listener).enterType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MiniJavaGrammarListener ) ((MiniJavaGrammarListener)listener).exitType(this);
		}
	}

	public final TypeContext type() throws RecognitionException {
		TypeContext _localctx = new TypeContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_type);
		try {
			setState(210);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,16,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(204);
				match(T__23);
				setState(205);
				match(T__11);
				setState(206);
				match(T__12);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(207);
				match(T__25);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(208);
				match(T__23);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(209);
				match(IDENTIFIER);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementContext extends ParserRuleContext {
		public Token IDENTIFIER;
		public ExpressionContext expression;
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode IDENTIFIER() { return getToken(MiniJavaGrammarParser.IDENTIFIER, 0); }
		public ArrayIndexContext arrayIndex() {
			return getRuleContext(ArrayIndexContext.class,0);
		}
		public StatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MiniJavaGrammarListener ) ((MiniJavaGrammarListener)listener).enterStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MiniJavaGrammarListener ) ((MiniJavaGrammarListener)listener).exitStatement(this);
		}
	}

	public final StatementContext statement() throws RecognitionException {
		StatementContext _localctx = new StatementContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_statement);
		int _la;
		try {
			setState(253);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,18,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(212);
				match(T__4);
				setState(216);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__4) | (1L << T__26) | (1L << T__28) | (1L << T__29) | (1L << IDENTIFIER))) != 0)) {
					{
					{
					setState(213);
					statement();
					}
					}
					setState(218);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(219);
				match(T__14);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(220);
				match(T__26);
				setState(221);
				match(T__9);
				setState(222);
				expression(0);
				setState(223);
				match(T__13);
				setState(224);
				statement();
				setState(225);
				match(T__27);
				setState(226);
				statement();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(228);
				match(T__28);
				setState(229);
				match(T__9);
				setState(230);
				expression(0);
				setState(231);
				match(T__13);
				setState(232);
				statement();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(234);
				match(T__29);
				setState(235);
				match(T__9);
				setState(236);
				expression(0);
				setState(237);
				match(T__13);
				setState(238);
				match(T__2);
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(240);
				((StatementContext)_localctx).IDENTIFIER = match(IDENTIFIER);
				setState(241);
				match(T__30);
				setState(242);
				((StatementContext)_localctx).expression = expression(0);
				setState(243);
				match(T__2);

				                ParameterDeclarationContext varContext = MyErrorListener.getVariableDeclarationFromExpression(_localctx,((StatementContext)_localctx).IDENTIFIER);
				                String variableType = MyErrorListener.checkDefined(varContext,((StatementContext)_localctx).IDENTIFIER);
				                MyErrorListener.assignStatementCheck(variableType,((StatementContext)_localctx).expression.expressionType,((StatementContext)_localctx).IDENTIFIER);
				            
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(246);
				((StatementContext)_localctx).IDENTIFIER = match(IDENTIFIER);
				setState(247);
				arrayIndex();
				setState(248);
				match(T__30);
				setState(249);
				((StatementContext)_localctx).expression = expression(0);
				setState(250);
				match(T__2);

				                ParameterDeclarationContext varContext = MyErrorListener.getVariableDeclarationFromExpression(_localctx,((StatementContext)_localctx).IDENTIFIER);
				                String variableType = MyErrorListener.checkDefined(varContext,((StatementContext)_localctx).IDENTIFIER);
				                MyErrorListener.validArrayVariable(variableType,((StatementContext)_localctx).IDENTIFIER);
				                MyErrorListener.assignStatementCheck("int",((StatementContext)_localctx).expression.expressionType,((StatementContext)_localctx).IDENTIFIER);
				            
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArrayIndexContext extends ParserRuleContext {
		public Token t;
		public Token token;
		public ExpressionContext expr;
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ArrayIndexContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arrayIndex; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MiniJavaGrammarListener ) ((MiniJavaGrammarListener)listener).enterArrayIndex(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MiniJavaGrammarListener ) ((MiniJavaGrammarListener)listener).exitArrayIndex(this);
		}
	}

	public final ArrayIndexContext arrayIndex() throws RecognitionException {
		ArrayIndexContext _localctx = new ArrayIndexContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_arrayIndex);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(255);
			((ArrayIndexContext)_localctx).token = match(T__11);
			setState(256);
			((ArrayIndexContext)_localctx).expr = expression(0);
			setState(257);
			match(T__12);

			                MyErrorListener.isInteger(((ArrayIndexContext)_localctx).expr.expressionType,((ArrayIndexContext)_localctx).token);
			                ((ArrayIndexContext)_localctx).t =  ((ArrayIndexContext)_localctx).token;
			            
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 7:
			return expression_sempred((ExpressionContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean expression_sempred(ExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 13);
		case 1:
			return precpred(_ctx, 12);
		case 2:
			return precpred(_ctx, 11);
		case 3:
			return precpred(_ctx, 10);
		case 4:
			return precpred(_ctx, 9);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3(\u0107\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\3\2\5\2\32\n\2\3\2\3\2\7\2\36\n\2\f\2\16\2!\13\2\3\2\3\2\3"+
		"\2\3\3\3\3\3\3\3\3\7\3*\n\3\f\3\16\3-\13\3\3\3\3\3\3\4\3\4\3\4\3\4\3\4"+
		"\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\7\4@\n\4\f\4\16\4C\13\4\3\4\3"+
		"\4\3\4\3\5\3\5\3\5\3\5\3\5\5\5M\n\5\3\5\3\5\7\5Q\n\5\f\5\16\5T\13\5\3"+
		"\5\7\5W\n\5\f\5\16\5Z\13\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\b"+
		"\3\b\3\b\3\b\3\b\3\b\3\b\7\bm\n\b\f\b\16\bp\13\b\5\br\n\b\3\b\3\b\3\b"+
		"\3\b\7\bx\n\b\f\b\16\b{\13\b\3\b\7\b~\n\b\f\b\16\b\u0081\13\b\3\b\3\b"+
		"\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3"+
		"\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\5\t"+
		"\u00a7\n\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t"+
		"\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\7\t\u00c2\n\t\f\t\16\t\u00c5"+
		"\13\t\5\t\u00c7\n\t\3\t\7\t\u00ca\n\t\f\t\16\t\u00cd\13\t\3\n\3\n\3\n"+
		"\3\n\3\n\3\n\5\n\u00d5\n\n\3\13\3\13\7\13\u00d9\n\13\f\13\16\13\u00dc"+
		"\13\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13"+
		"\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13"+
		"\3\13\3\13\3\13\3\13\3\13\3\13\3\13\5\13\u0100\n\13\3\f\3\f\3\f\3\f\3"+
		"\f\3\f\2\3\20\r\2\4\6\b\n\f\16\20\22\24\26\2\2\2\u011e\2\31\3\2\2\2\4"+
		"%\3\2\2\2\6\60\3\2\2\2\bG\3\2\2\2\n^\3\2\2\2\fb\3\2\2\2\16e\3\2\2\2\20"+
		"\u00a6\3\2\2\2\22\u00d4\3\2\2\2\24\u00ff\3\2\2\2\26\u0101\3\2\2\2\30\32"+
		"\5\4\3\2\31\30\3\2\2\2\31\32\3\2\2\2\32\33\3\2\2\2\33\37\5\6\4\2\34\36"+
		"\5\b\5\2\35\34\3\2\2\2\36!\3\2\2\2\37\35\3\2\2\2\37 \3\2\2\2 \"\3\2\2"+
		"\2!\37\3\2\2\2\"#\7\2\2\3#$\b\2\1\2$\3\3\2\2\2%&\7\3\2\2&+\7\'\2\2\'("+
		"\7\4\2\2(*\7\'\2\2)\'\3\2\2\2*-\3\2\2\2+)\3\2\2\2+,\3\2\2\2,.\3\2\2\2"+
		"-+\3\2\2\2./\7\5\2\2/\5\3\2\2\2\60\61\7\6\2\2\61\62\7\'\2\2\62\63\7\7"+
		"\2\2\63\64\7\b\2\2\64\65\7\t\2\2\65\66\7\n\2\2\66\67\7\13\2\2\678\7\f"+
		"\2\289\7\r\2\29:\7\16\2\2:;\7\17\2\2;<\7\'\2\2<=\7\20\2\2=A\7\7\2\2>@"+
		"\5\24\13\2?>\3\2\2\2@C\3\2\2\2A?\3\2\2\2AB\3\2\2\2BD\3\2\2\2CA\3\2\2\2"+
		"DE\7\21\2\2EF\7\21\2\2F\7\3\2\2\2GH\7\6\2\2HI\7\'\2\2IL\b\5\1\2JK\7\22"+
		"\2\2KM\7\'\2\2LJ\3\2\2\2LM\3\2\2\2MN\3\2\2\2NR\7\7\2\2OQ\5\f\7\2PO\3\2"+
		"\2\2QT\3\2\2\2RP\3\2\2\2RS\3\2\2\2SX\3\2\2\2TR\3\2\2\2UW\5\16\b\2VU\3"+
		"\2\2\2WZ\3\2\2\2XV\3\2\2\2XY\3\2\2\2Y[\3\2\2\2ZX\3\2\2\2[\\\7\21\2\2\\"+
		"]\b\5\1\2]\t\3\2\2\2^_\5\22\n\2_`\7\'\2\2`a\b\6\1\2a\13\3\2\2\2bc\5\n"+
		"\6\2cd\7\5\2\2d\r\3\2\2\2ef\7\b\2\2fg\5\n\6\2gh\b\b\1\2hq\7\f\2\2in\5"+
		"\n\6\2jk\7\4\2\2km\5\n\6\2lj\3\2\2\2mp\3\2\2\2nl\3\2\2\2no\3\2\2\2or\3"+
		"\2\2\2pn\3\2\2\2qi\3\2\2\2qr\3\2\2\2rs\3\2\2\2st\7\20\2\2tu\b\b\1\2uy"+
		"\7\7\2\2vx\5\f\7\2wv\3\2\2\2x{\3\2\2\2yw\3\2\2\2yz\3\2\2\2z\177\3\2\2"+
		"\2{y\3\2\2\2|~\5\24\13\2}|\3\2\2\2~\u0081\3\2\2\2\177}\3\2\2\2\177\u0080"+
		"\3\2\2\2\u0080\u0082\3\2\2\2\u0081\177\3\2\2\2\u0082\u0083\7\23\2\2\u0083"+
		"\u0084\5\20\t\2\u0084\u0085\7\5\2\2\u0085\u0086\7\21\2\2\u0086\u0087\b"+
		"\b\1\2\u0087\17\3\2\2\2\u0088\u0089\b\t\1\2\u0089\u008a\7\'\2\2\u008a"+
		"\u00a7\b\t\1\2\u008b\u008c\7(\2\2\u008c\u00a7\b\t\1\2\u008d\u008e\7\26"+
		"\2\2\u008e\u00a7\b\t\1\2\u008f\u0090\7\27\2\2\u0090\u00a7\b\t\1\2\u0091"+
		"\u0092\7\30\2\2\u0092\u00a7\b\t\1\2\u0093\u0094\7\31\2\2\u0094\u0095\7"+
		"\32\2\2\u0095\u0096\5\26\f\2\u0096\u0097\b\t\1\2\u0097\u00a7\3\2\2\2\u0098"+
		"\u0099\7\31\2\2\u0099\u009a\7\'\2\2\u009a\u009b\7\f\2\2\u009b\u009c\7"+
		"\20\2\2\u009c\u00a7\b\t\1\2\u009d\u009e\7\33\2\2\u009e\u009f\5\20\t\4"+
		"\u009f\u00a0\b\t\1\2\u00a0\u00a7\3\2\2\2\u00a1\u00a2\7\f\2\2\u00a2\u00a3"+
		"\5\20\t\2\u00a3\u00a4\7\20\2\2\u00a4\u00a5\b\t\1\2\u00a5\u00a7\3\2\2\2"+
		"\u00a6\u0088\3\2\2\2\u00a6\u008b\3\2\2\2\u00a6\u008d\3\2\2\2\u00a6\u008f"+
		"\3\2\2\2\u00a6\u0091\3\2\2\2\u00a6\u0093\3\2\2\2\u00a6\u0098\3\2\2\2\u00a6"+
		"\u009d\3\2\2\2\u00a6\u00a1\3\2\2\2\u00a7\u00cb\3\2\2\2\u00a8\u00a9\f\17"+
		"\2\2\u00a9\u00aa\7%\2\2\u00aa\u00ab\5\20\t\20\u00ab\u00ac\b\t\1\2\u00ac"+
		"\u00ca\3\2\2\2\u00ad\u00ae\f\16\2\2\u00ae\u00af\7&\2\2\u00af\u00b0\5\20"+
		"\t\17\u00b0\u00b1\b\t\1\2\u00b1\u00ca\3\2\2\2\u00b2\u00b3\f\r\2\2\u00b3"+
		"\u00b4\5\26\f\2\u00b4\u00b5\b\t\1\2\u00b5\u00ca\3\2\2\2\u00b6\u00b7\f"+
		"\f\2\2\u00b7\u00b8\7\24\2\2\u00b8\u00b9\7\25\2\2\u00b9\u00ca\b\t\1\2\u00ba"+
		"\u00bb\f\13\2\2\u00bb\u00bc\7\24\2\2\u00bc\u00bd\7\'\2\2\u00bd\u00c6\7"+
		"\f\2\2\u00be\u00c3\5\20\t\2\u00bf\u00c0\7\4\2\2\u00c0\u00c2\5\20\t\2\u00c1"+
		"\u00bf\3\2\2\2\u00c2\u00c5\3\2\2\2\u00c3\u00c1\3\2\2\2\u00c3\u00c4\3\2"+
		"\2\2\u00c4\u00c7\3\2\2\2\u00c5\u00c3\3\2\2\2\u00c6\u00be\3\2\2\2\u00c6"+
		"\u00c7\3\2\2\2\u00c7\u00c8\3\2\2\2\u00c8\u00ca\7\20\2\2\u00c9\u00a8\3"+
		"\2\2\2\u00c9\u00ad\3\2\2\2\u00c9\u00b2\3\2\2\2\u00c9\u00b6\3\2\2\2\u00c9"+
		"\u00ba\3\2\2\2\u00ca\u00cd\3\2\2\2\u00cb\u00c9\3\2\2\2\u00cb\u00cc\3\2"+
		"\2\2\u00cc\21\3\2\2\2\u00cd\u00cb\3\2\2\2\u00ce\u00cf\7\32\2\2\u00cf\u00d0"+
		"\7\16\2\2\u00d0\u00d5\7\17\2\2\u00d1\u00d5\7\34\2\2\u00d2\u00d5\7\32\2"+
		"\2\u00d3\u00d5\7\'\2\2\u00d4\u00ce\3\2\2\2\u00d4\u00d1\3\2\2\2\u00d4\u00d2"+
		"\3\2\2\2\u00d4\u00d3\3\2\2\2\u00d5\23\3\2\2\2\u00d6\u00da\7\7\2\2\u00d7"+
		"\u00d9\5\24\13\2\u00d8\u00d7\3\2\2\2\u00d9\u00dc\3\2\2\2\u00da\u00d8\3"+
		"\2\2\2\u00da\u00db\3\2\2\2\u00db\u00dd\3\2\2\2\u00dc\u00da\3\2\2\2\u00dd"+
		"\u0100\7\21\2\2\u00de\u00df\7\35\2\2\u00df\u00e0\7\f\2\2\u00e0\u00e1\5"+
		"\20\t\2\u00e1\u00e2\7\20\2\2\u00e2\u00e3\5\24\13\2\u00e3\u00e4\7\36\2"+
		"\2\u00e4\u00e5\5\24\13\2\u00e5\u0100\3\2\2\2\u00e6\u00e7\7\37\2\2\u00e7"+
		"\u00e8\7\f\2\2\u00e8\u00e9\5\20\t\2\u00e9\u00ea\7\20\2\2\u00ea\u00eb\5"+
		"\24\13\2\u00eb\u0100\3\2\2\2\u00ec\u00ed\7 \2\2\u00ed\u00ee\7\f\2\2\u00ee"+
		"\u00ef\5\20\t\2\u00ef\u00f0\7\20\2\2\u00f0\u00f1\7\5\2\2\u00f1\u0100\3"+
		"\2\2\2\u00f2\u00f3\7\'\2\2\u00f3\u00f4\7!\2\2\u00f4\u00f5\5\20\t\2\u00f5"+
		"\u00f6\7\5\2\2\u00f6\u00f7\b\13\1\2\u00f7\u0100\3\2\2\2\u00f8\u00f9\7"+
		"\'\2\2\u00f9\u00fa\5\26\f\2\u00fa\u00fb\7!\2\2\u00fb\u00fc\5\20\t\2\u00fc"+
		"\u00fd\7\5\2\2\u00fd\u00fe\b\13\1\2\u00fe\u0100\3\2\2\2\u00ff\u00d6\3"+
		"\2\2\2\u00ff\u00de\3\2\2\2\u00ff\u00e6\3\2\2\2\u00ff\u00ec\3\2\2\2\u00ff"+
		"\u00f2\3\2\2\2\u00ff\u00f8\3\2\2\2\u0100\25\3\2\2\2\u0101\u0102\7\16\2"+
		"\2\u0102\u0103\5\20\t\2\u0103\u0104\7\17\2\2\u0104\u0105\b\f\1\2\u0105"+
		"\27\3\2\2\2\25\31\37+ALRXnqy\177\u00a6\u00c3\u00c6\u00c9\u00cb\u00d4\u00da"+
		"\u00ff";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}