package gen;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;

/**
 * @program: minijava-compiler
 * @description:
 * @author: Erika
 * @create: 2018-12-26 11:14
 **/
public class myListener implements MiniJavaGrammarListener{

    @Override
    public void enterGoal(MiniJavaGrammarParser.GoalContext ctx) {

    }

    @Override
    public void exitGoal(MiniJavaGrammarParser.GoalContext ctx) {

    }

    @Override
    public void enterPackageDeclaration(MiniJavaGrammarParser.PackageDeclarationContext ctx) {

    }

    @Override
    public void exitPackageDeclaration(MiniJavaGrammarParser.PackageDeclarationContext ctx) {

    }

    @Override
    public void enterMainClass(MiniJavaGrammarParser.MainClassContext ctx) {

    }

    @Override
    public void exitMainClass(MiniJavaGrammarParser.MainClassContext ctx) {

    }

    @Override
    public void enterClassDeclaration(MiniJavaGrammarParser.ClassDeclarationContext ctx) {

    }

    @Override
    public void exitClassDeclaration(MiniJavaGrammarParser.ClassDeclarationContext ctx) {

    }

    @Override
    public void enterParameterDeclaration(MiniJavaGrammarParser.ParameterDeclarationContext ctx) {

    }

    @Override
    public void exitParameterDeclaration(MiniJavaGrammarParser.ParameterDeclarationContext ctx) {

    }

    @Override
    public void enterVarDeclarationStatement(MiniJavaGrammarParser.VarDeclarationStatementContext ctx) {

    }

    @Override
    public void exitVarDeclarationStatement(MiniJavaGrammarParser.VarDeclarationStatementContext ctx) {

    }

    @Override
    public void enterMethodDeclaration(MiniJavaGrammarParser.MethodDeclarationContext ctx) {

    }

    @Override
    public void exitMethodDeclaration(MiniJavaGrammarParser.MethodDeclarationContext ctx) {

    }

    @Override
    public void enterExpression(MiniJavaGrammarParser.ExpressionContext ctx) {
        ctx.expression(0);
    }

    @Override
    public void exitExpression(MiniJavaGrammarParser.ExpressionContext ctx) {

    }

    @Override
    public void enterType(MiniJavaGrammarParser.TypeContext ctx) {

    }

    @Override
    public void exitType(MiniJavaGrammarParser.TypeContext ctx) {

    }

    @Override
    public void enterStatement(MiniJavaGrammarParser.StatementContext ctx) {

    }

    @Override
    public void exitStatement(MiniJavaGrammarParser.StatementContext ctx) {

    }

    @Override
    public void enterArrayIndex(MiniJavaGrammarParser.ArrayIndexContext ctx) {

    }

    @Override
    public void exitArrayIndex(MiniJavaGrammarParser.ArrayIndexContext ctx) {

    }

    @Override
    public void visitTerminal(TerminalNode terminalNode) {

    }

    @Override
    public void visitErrorNode(ErrorNode errorNode) {

    }

    @Override
    public void enterEveryRule(ParserRuleContext parserRuleContext) {

    }

    @Override
    public void exitEveryRule(ParserRuleContext parserRuleContext) {

    }
}

