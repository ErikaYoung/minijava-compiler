grammar MiniJavaGrammar;

@header{
	import gen.MyErrorListener;
}

goal: (packageDeclaration)? mainClass (classDeclaration)* EOF{
        MyErrorListener.getAllExpression($ctx);
      };

packageDeclaration: 'package' IDENTIFIER(','IDENTIFIER)*';';

mainClass: 'class' className = IDENTIFIER '{''public''static''void''main''(''String''['']'IDENTIFIER')''{'statement*'}''}';

classDeclaration
locals[String name]: 'class' IDENTIFIER{
                        $name = $IDENTIFIER.text;
                        }
                       ('extends' extendClass = IDENTIFIER)?'{'(varDeclarationStatement)*(methodDeclaration)*'}'{
                            MyErrorListener.checkClassName($ctx,$IDENTIFIER);
                            if($extendClass!=null)

                                MyErrorListener.getClassDeclaration($ctx,$extendClass.getText(),$IDENTIFIER);

                       };

parameterDeclaration
locals[String varType,String varName]: t=type IDENTIFIER {
    $varType = $t.text;
    $ctx.varName = $IDENTIFIER.text;
    MyErrorListener.checkVariableDefined($ctx,$IDENTIFIER);
};

varDeclarationStatement:parameterDeclaration a=';';

methodDeclaration
locals[String returnType]: 'public' method=parameterDeclaration{
                            $returnType = ((MethodDeclarationContext)_localctx).method.varType;
                     }c='('(parameterDeclaration(','parameterDeclaration)*)?')'{
                            MyErrorListener.methodOverrideCheck($ctx,$c);
                     }'{'(varDeclarationStatement)*(statement)*'return'expression';''}'{
                          MyErrorListener.returnTypeCheck($returnType,$expression.expressionType,$c);

                    };

expression returns[String expressionType]:
            IDENTIFIER{
                ParameterDeclarationContext varContext = MyErrorListener.getVariableDeclarationFromExpression($ctx,$IDENTIFIER);
                $expressionType = MyErrorListener.checkDefined(varContext,$IDENTIFIER);
            }
            | exp1 = expression op = INTEGER_OPERATOR exp2 = expression{
                MyErrorListener.validIntegerOperation($exp1.expressionType,$exp2.expressionType, $op);
                $expressionType = "int";
            }
            | exp1 = expression op = BOOLEAN_OPERATOR exp2 = expression{
                MyErrorListener.validBooleanOperation($exp1.expressionType,$exp2.expressionType, $op);
                $expressionType = "boolean";
            }
            | exp = expression arr = arrayIndex {
                MyErrorListener.validArrayVariable($exp.expressionType,$arr.t);
                $expressionType ="int";
            }
            | exp1=expression '.' op='length'{
                MyErrorListener.validLengthOperation($exp1.expressionType, $op);
                $expressionType = "int";
            }
            | var=expression '.' IDENTIFIER '('(expression ( ',' expression )* )? ')'
            | INTEGER
            {
                $expressionType = "int";
            }
            | 'true'
            {
                $expressionType = "boolean";
            }
            | 'false'
            {
                $expressionType = "boolean";
            }
            | 'this'{
                $expressionType = MyErrorListener.getCurrentClass($ctx);
            }
            | 'new''int' arrayIndex{
                $expressionType = "int[]";
            }
            | 'new' token = IDENTIFIER '('')'{
                $expressionType = $token.getText();
            }
            | '!' exp = expression{
                $expressionType = $exp.expressionType;
            }
            | '('exp = expression')'{
                $expressionType = $exp.expressionType;
            };

type:	'int' '[' ']'
        |  'boolean'
        |  'int'
        | IDENTIFIER;

statement: '{' (statement)* '}'
            | 'if''(' expression ')' statement 'else' statement
            | 'while''('expression')'statement
            | 'System.out.println''('expression')'';'
            | IDENTIFIER '=' expression';'{
                ParameterDeclarationContext varContext = MyErrorListener.getVariableDeclarationFromExpression($ctx,$IDENTIFIER);
                String variableType = MyErrorListener.checkDefined(varContext,$IDENTIFIER);
                MyErrorListener.assignStatementCheck(variableType,$expression.expressionType,$IDENTIFIER);
            }
            | IDENTIFIER arrayIndex '='expression';'{
                ParameterDeclarationContext varContext = MyErrorListener.getVariableDeclarationFromExpression($ctx,$IDENTIFIER);
                String variableType = MyErrorListener.checkDefined(varContext,$IDENTIFIER);
                MyErrorListener.validArrayVariable(variableType,$IDENTIFIER);
                MyErrorListener.assignStatementCheck("int",$expression.expressionType,$IDENTIFIER);
            };

arrayIndex returns[Token t]: token='['expr=expression']'
            {
                MyErrorListener.isInteger($expr.expressionType,$token);
                $t = $token;
            };


SPACE: [ \t\n\r]+ ->  skip;
ONELINE_COMMENT: '//' .*? '\n' -> skip;
MULTIPLELINE_COMMENT: '/*' .*? '*/' -> skip;

INTEGER_OPERATOR: '<' | '+' | '-' | '*';
BOOLEAN_OPERATOR: '&&';

IDENTIFIER: [a-zA-Z_][0-9a-zA-Z_]*;
INTEGER:  [0-9]+;

